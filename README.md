 1. [Genouest data structure organization](#genouest-data-structure-organization)
 2. [canFam4, canFam5, ...](#canfam4-canfam5-)
 3. [Ortholog genes](#ortholog-genes)
 4. [Hopefully useful tools?](#hopefully-useful-tools)
 5. [Canfam3 extended annotation track](#canfam3-extended-annotation-track)
6. [Make genocluster data accessible for external users using HomeAndCo](#make-genocluster-data-accessible-for-external-users-using-homeandco)
 
## Genouest data structure organization
Data organization on [genouest bioinformatic platform](https://www.genouest.org) and (hopefully) useful tools.   
Feel free to check many useful informations on [genouest usage](https://help.genouest.org/usage/cluster/#storage).

Our `$HOME` are located in `/home/genouest/cnrs_umr6290/` and we have a private storage here : `/groups/dog/`.

Then useful canine resources could be accessed here :

**Dog reference genome sequences:**
- canFam4+ Roslind chrY  :`/groups/dog/data/canFam4/sequence/UU_Cfam_GSD_1.0_canFam4/UU_Cfam_GSD_1.0_ROSY.fa`
- Tasha10k : `/groups/dog/data/Tasha4.0/sequence/Tasha4.renamed.fasta.gz`
- canFam3 : `/groups/dog/data/canFam3/sequence/softmasked/Canis_familiaris.CanFam3.1.72.dna_sm.toplevel.fa`

**Dog genome annotations :**
- canFam4+ Roslind chrY (dog10k file)  : `/groups/dog/data/canFam4/annotation/refseq/refseq_with_chrY.dir/UU_Cfam_GSD_1.0_ROSY.refSeq.with_chr.gtf`
- canFam4+ Roslind chrY + 5433 pseudogene  : `/groups/dog/data/canFam4/annotation/refseq/refseq_with_chrY.dir/UU_Cfam_GSD_1.0_ROSY.refSeq.with_chr.CurGen.pseudo.gtf`
  - **Bonus** : CAGE TSSs from DogA consortium (robust or comprehensive): `/groups/dog/data/canFam4/annotation/DoGA_TSS/`
    - merged across 116 tissues = `canFam4_dog_TSSs*bed`
    - by individuals tissues = `TSSs_*.tsv`
- canFam3 version Ensembl (v99) :`/groups/dog/data/canFam3/annotation/Ensembl99/Canis_familiaris.CanFam3.1.99.gtf`
- canFam3 version 3.2 (Wucher, 2017): `/groups/dog/data/canFam3/annotation/canfam3.2/canfam3.2.gtf` # on [UCSC track hub](http://tools.genouest.org/data/tderrien/canFam3.1p2/annotation/trackhub2/canfam3.1p_trackhub/hub.txt)



**NGS Data**

Then data related to (dog) NGS project are in `/groups/dog/data/canFam3/NGS/` and the structure is as followed:    

```
=> PROJECT_NAME : e.g. Melanoma ou Epilepsy
	=> DNA | RNA : biological material 
		 => Sequencing_type : *new* : could be WGS (Whole Genome Sequencing); EXOME ; Targeted_Sequencing; ou ./ (si q’un seul type de séquençage)
			=> BAM : BAM Files
			=> FASTQ : FASTQ Files
			=> RESULTS : Results linked to analysis on the project
				=> version d’annotation	: may depend on the used genome annotation version on_canfam3 (Ensembl) ou on_canfam3.2
```

For instance, the data structure is as followed (July 2020):
```
$ tree -L 3 -d ./
|-- dog10k
|   `-- DNA
|       |-- BAM
|       |-- FASTQ
|       |-- RESULTS
|       `-- scripts
|-- Dog_Reannotation
|   |-- DNA
|   `-- RNA
|       |-- BAM
|       |-- FASTQ
|       `-- RESULTS
```

## canFam4, canFam5, ...

Given the increasing number of novel dog genome assemblies (and subsequent `.gtf` annotations), novel (sub-)directories are created on genouest (*e.g.* canFam4):
`/groups/dog/data/canFam4/`

### canFam4 annotation

The different assemblies are noticed here :
  - for NCBI, `ROS_Cfam_1.0` is considered to be the **reference** 
  - for the dog community (dog10k) `UU_Cfam_GSD_1.0` + the chrY of is `ROS_Cfam_1.0` the reference...
```
Breed               Assembly_name       Assembly_accession  Submitter                         Assembly_date  Reference/Alternate  Assembly_content
Labrador_retriever  ROS_Cfam_1.0        GCF_014441545.1     The_Roslin_Institute              09-03-2020     Reference            40_assembled_chromosomes;_unplaced_scaffolds
Great_Dane          UMICH_Zoey_3.1      GCF_005444595.1     University_of_Michigan            05-30-2019     Alternate            39_assembled_chromosomes;_unplaced_scaffolds
Basenji             UNSW_CanFamBas_1.0  GCF_013276365.1     University_of_New_South_Wales     06-11-2020     Alternate            39_assembled_chromosomes;_unplaced_scaffolds
German_Shepherd*     UU_Cfam_GSD_1.0     GCF_011100685.1     Uppsala_University                03-10-2020     Reference            39_assembled_chromosomes;_unplaced_scaffolds
Boxer               Dog10K_Boxer_Tasha  GCF_000002285.5     Dog_Genome_Sequencing_Consortium  10-06-2020     Alternate            40_assembled_chromosomes;_unplaced_scaffolds
```

For refseq, a comparison of gene/tx counts between dog assemblies is available [here](https://www.ncbi.nlm.nih.gov/genome/annotation_euk/Canis_lupus_familiaris/106/) (Thanks Matthias!). 

**Note**:

Refseq has a specific convention for naming chromosomes in both the `.gtf` and `.fa` with a [a corresponding file](https://ftp.ncbi.nlm.nih.gov/genomes/all/annotation_releases/9615/106/GCF_011100685.1_UU_Cfam_GSD_1.0/GCF_011100685.1_UU_Cfam_GSD_1.0_assembly_report.txt). For information, the Refseq `.gtf` file has been converted to ensembl chrosome names here:
`/groups/dog/data/canFam4/annotation/refseq/GCF_011100685.1_UU_Cfam_GSD_1.0_genomic_chrEns.gtf`


### canFam4 variations

#### SNPs and IndDels

  * **Dog10K**

Data containing **indels and SNP callsets** for the 1,987 samples (depth>10x) processed by the Dog10K Consortium are here 
```
/groups/dog/data/canFam4/variation/VCF.dir/
```
More information on the process in the `callset.README.txt` file.

* **DBVDC**
```
 /groups/dog/data/canFam4/variation/DBVDC_cohort/
```

#### SV calls
For **Dog10K**, it is available here :
```
/groups/dog/data/canFam4/variation/VCF.dir/SV-genotype*
```
More information in the `svcallset.README.txt` file.

#### LiftOver from canFam3 to canFam4

To map a `.bed` file from canFam3 to canFam4, one can use the liftOver binary executable and a chain file chaining the 2 assemblies:
`/groups/dog/data/canFam4/liftover/canFam3ToCanFam4.over.chain.gz`

Usage :
```
CHAINFILE="/groups/dog/data/canFam4/liftover/canFam3ToCanFam4.over.chain.gz"
~tderrien/progs/UCSC/exec/liftOver myFILE_onCf3.bed $CHAINFILE myFILE_onCf4.bed myFILE_onCf3.unmapped
```

## Ortholog genes

Extracted from Biomart 104 and 105.

- canFam3 = `/groups/dog/data/canFam3/ortholog_biomart/`
- canFam4 = `/groups/dog/data/canFam4/ortholog_biomart/`

## Hopefully useful tools


On genouest machines, some tools can be found here :
 - file format parser :  `~tderrien/bin/convert`
 - usefuls scripts from the team : `/groups/dog/script/ # (need to be reorganized)`
 - after connexion on a specific node : `source /local/env/env_PROGNAME` # https://help.genouest.org/usage/slurm/
 
 
## Canfam3 extended annotation track
You can load the canFam3.2 gene annotation track (Wucher et al, 2017) on [UCSC genome browser](http://genome.ucsc.edu/cgi-bin/hgGateway?clade=mammal&org=Dog) :

- select top panel *My Data > Track Hub*
- click *My Hubs* and copy/paste the URL `http://tools.genouest.org/data/tderrien/canFam3.1p2/annotation/trackhub2/canfam3.1p_trackhub/hub.txt`
- click *Add Hub*


or more directly, dwonload the GTF file including lincRNAs from the FEELnc here: 
http://tools.genouest.org/data/tderrien/canfam3.2.UCSC.gtf

On genouest :
 - the `.gtf` file located here : 
`/groups/dog/data/canFam3/annotation/canfam3.2/canfam3_cons_annot_TritouPuppetMasterChief_23-03-2016_lncClasse_geneBiotype_withEnsCds_withSilicoCds_withGoodGeneSource_gene_biotype_woCDS.gtf`
- the index file (with ESCAFG and gene_name) located here : `/groups/dog/data/canFam3/annotation/canfam3.2/RLOC_ENSCAF_GnNAME_Biotype.index`

## Make genocluster data accessible for external users using HomeAndCo:

Given the recurrent synchronisation issue with [data-acces](https://data-access.cesgo.org/), we now use [HomeAndCo](https://homeandco.genouest.org/) to create automatic URL to make genouest file downloadable from outside.

```
putonHomeAndCo.sh <path/to/file>
```
Notes:
- It requires the Genouest pwd
- is also possible to do it by directly clicking on the file using the [HomeAndCo](https://homeandco.genouest.org/) website.


Another simple alternative is to create a file `homeandco.public` in the dedicated dir and then extract with `web` the path to the file. See [HomeAndCo](https://homeandco.genouest.org/) > DOC > Managing access.

URL shoul be formated as: https://homeandco.genouest.org/web/path_to_file

:warning: anyone with url can view it