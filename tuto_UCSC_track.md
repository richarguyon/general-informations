## Tuto to create UCSC track


**Goal: Create UCSC track from gtf file**


### 1- Reformat GTF file to UCSC format

- retrieve exons only
- if ensembl format: add ***chr*** for each chromosome and replace MT by chrM
- remove unknown chomosomes

Example if gtf in Ensembl format:
``` {
awk '$3 == "exon" {print "chr"$0}' extended_annotations.filter.gtf | sed 's/chrMT/chrM/g' | grep -v chrKI27 | grep -v chrGL000 > extended_annotations.filter_exonsChr.gtf 
```

Example if gtf in UCSC format:
``` {
awk '$3 == "exon" {print $0}' extended_annotations.filter.gtf | grep -v chrKI27 | grep -v chrGL000 > extended_annotations.filter_exons.gtf 
```
 

### 2- Add gtf informations and track features on GTF file

- add column informations
- add track informations to view on browser: track name and description, color...

```{
sed '1s/^/#seqname source feature start end score strand frame attribute\n/' extended_annotations.filter_exonsChr.gtf | \
sed '1s/^/track name="lnaRNAresist_annexa_robust" description="GTF format custom track. Filtered output file (robust) from annexa in lncRNA resist project" color=0,176,80\n/' extended_annotations.filter_exonsChr.gtf > extended_annotations.filter_exonsChr_track.gtf 
```


### 3- Load UCSC formatted GTF file on browser

On [UCSC browser](https://genome-euro.ucsc.edu/cgi-bin/hgCustom?hgsid=294441662_AlhYtx54qi8xtEFROD6A2bZL7Www) :
- choose species and version of assembly
- upload your formatted GTF file or copy URL
- submit the file
- save it


### 4- Usage/Share

- track saved in ***My Data/My sessions***
- click on ***Email*** to share the track
- click on track to load the track on UCSC browser -> the track is available and modifiable in ***Custom Tracks*** 


### NOTE: how to create URL 

- create URL link using HomeAndCo from Genouest (put your genouest password)
```{
~tderrien/bin/putonHomeAndCo.sh extended_annotations.filter_exonsChr_track.gtf
```
- copy/paste URL link starting with ***https://homeandco.genouest.org/api/download/***
