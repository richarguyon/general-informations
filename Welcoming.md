# Welcoming new biologeeks :-)

This markdown is meant to recapitulate the different steps neded to welcome new (biologeek) colleagues.

## ToDo list and resources:

- [ ] Create a [Genouest account](https://my.genouest.org/manager2/login) with team = `cnrs_umr6290`.
  - [ ] :warning: Read the [doc](https://help.genouest.org/usage/cluster/#cluster) and the [faq](https://help.genouest.org/faq/).
- [ ] Create a [gitlab](https://gitlab.com/users/sign_in) and [github](https://github.com/login) accounts (to be included in the ≠ projects).
- [ ] Create a [Zotero account](https://www.zotero.org/user/login/).
- [ ] Create a [discord account](https://discord.com/).
- [ ] Follow the bioinformatic [literature/advices](https://twitter.com/nilshomer/status/1511586109658046474?s=20&t=9Wb-3HXTMI2uWyUzF0PCMQ) :)
- [ ] Install [VSCode](https://code.visualstudio.com/) as a possible IDE : see [Tuto [FR]](./tuto_vscode/Tuto_VSCToJupyter.ipynb).

Join the groups:
- [ ] [Canine Genetic teamn](https://igdr.univ-rennes.fr/en/equipe-genetique-du-chien) 
  - [ ] Join the dedicated discord. 
  - [ ] Ask to be part part of Canine Genetic Team mailing list (<equipe-genetique-chien@listes.univ-rennes1.fr>)
  - [ ] Join the CGT google calendar (login : chien.igdr@gmail.com).
- [ ] [IGDRion](https://igdr.univ-rennes.fr/igdrion) 
  - [ ] Join the dedicated discord.
  - [ ] Join the [GitHub](https://github.com/igdrion).
- [ ] [BIS2 group](https://igdr.univ-rennes.fr/en/silico-biology) 
  - [ ] Join the dedicated discord.
  - [ ] Ask to be part part of BIS2 mailing list (igdr-bis@univ-rennes.fr)

## Data organization on Genouest

Sequencing data together with reference genome (`.fa`) and transcriptome (`.gtf/.bed`) assemblies are available on our Genouest partition.

More information is available [here](https://gitlab.com/bioinfog/general-informations).
