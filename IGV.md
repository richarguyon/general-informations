# Visualize Genouest data with IGV locally

Here are the ≠ steps.

## Mount Genouest locally (on Mac)

- Download and install [OSXFuse](https://osxfuse.github.io/) and `sshfs`.
  - `brew install --cask igv`
- (Reboot the Mac).
- Mount on specific Genouest folder *e.g.* :
```bash
GENODIR="/groups/dog/nanopore/" # genouest folder
LOCALDIR="genonanopore"       # local folder
```
Then mount ungin sshfs:
```bash
mkdir $LOCALDIR
sshfs tderrien@genossh.genouest.org:$GENODIR $LOCALDIR \
-oauto_cache,reconnect,defer_permissions,noappledouble,volname=$LOCALDIR,IdentityFile=/Users/admin/.ssh/id_rsa
```

## IGV

- Download and install [IGV](https://igv.org/doc/desktop/#DownloadPage/).
- Launch by [command-line](https://github.com/igvteam/igv/wiki/Command-Line) (for Mac):
  - on a specific locus `-l`
  - for a specific genome assembly `-g`
  - with multiple `bam` files mounted from Genouest.
  
  `bash igv.command -g hg38 -l chr1:112911847-112957593 ~/genonanopore/lncrna_plast/primary/human/501Mel_Naive_KO_Antti_*/drna_SQK-RNA002/flowcell/guppy_6.3.9/bam/hg38/bam/*bam`

  or

```bash
BAMLST=~/genonanopore/primary/dog/*/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/*.sorted.bam

bash /Applications/IGV_2.17.0/igv.command -g canFam4 -l chr10:2165619-2170946 $BAMLST
```

_PS: For IGV memory issues, uncomment the `$HOME/.igv/java_arguments` file with the desired memory_
