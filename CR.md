## 2023 Janv 9

First Lab meeting of the year :-)

- Slides for Genouest presentation is [here](https://uniren1-my.sharepoint.com/:p:/g/personal/thomas_derrien_univ-rennes1_fr/ETDX_wkuC5dBhOKp9JrLNDQB5beQhnB5EZiTeuBNIQ-0Iw?e=c4oJ6r)
- Using jupyter notebook [here](./221104_tuto_jupyterNotebook_genouestCluster.ipynb)

